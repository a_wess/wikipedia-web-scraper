#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 18:24:02 2019

@author: Andrew Wess
"""

from bs4 import BeautifulSoup
import urllib.request
from urllib.request import urlopen
import time
import requests


"""copy random wikipedia page's text to a text file"""
def scrape(n):
    
    file = open("wiki.txt","w") #output file
    
    for i in range(n):
        soup = BeautifulSoup(requests.get('https://www.wikipedia.org/wiki/Special:Random').content, 'html.parser')
        print("[" + str(i) + "]")
        print(soup.find("h1").text)
        file.write(soup.find("h1").text + "\n") # title
        for p in soup.find_all(['p', 'h']):
                print(p.text + "\n Text is available under the Creative Commons Attribution-ShareAlike License; additional terms may apply. By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization.Sourced from Wikipedia") #all paragraphs
                file.write(p.text +"\n Text is available under the Creative Commons Attribution-ShareAlike License; additional terms may apply. By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Sourced from Wikipedia")
     
        print("\nŒ\n")
        file.write("\nŒ\n") #end symbol, signifies the end of the article
        time.sleep(2)
        
    file.close()
