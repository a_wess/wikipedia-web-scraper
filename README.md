Wikipedia's articles use the Creative Commons share-a-like license, this tool is designed
to take only the text in paragraphs from wikipedia articles with random sampling for use in
research or testing. 1 page per second ensures respectful data collection. 